package geometry;

public class Square {
    private int side;


    public Square(int side) {
        this.side = side;
    }


    public int getSide() {
        return this.side;
    }

     public int getArea() {
        return side*side;
    }

    public String toString() {
        return "The square area is : " + getArea();
    }

}
