package geometry;
public class Shapes 
{
    public static void main( String[] args )
    {
        // Initializing Square
        Square sqr = new Square(5);
        System.out.println(sqr);

        Rectangle rect = new Rectangle(7, 11);
        System.out.println(rect.toString());
    }
}
