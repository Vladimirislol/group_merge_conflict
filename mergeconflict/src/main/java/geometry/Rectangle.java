package geometry;

public class Rectangle {
    private int length;
    private int width;
    private int area;

    public Rectangle(int side1, int side2) {
        this.length = side1;
        this.width = side2;
        this.area = side1*side2;
    }

    public int getLength() {
        return this.length;
    }

    public int getWidth() {
        return this.width;
    }

    public int getArea() {
        return this.area;
    }

    public String toString() {
        String result = "length: "+this.length+", width: "+this.width+", area: "+this.area;
        return result;
    }
}
