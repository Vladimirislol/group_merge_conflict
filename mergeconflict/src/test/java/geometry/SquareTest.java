package geometry;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import org.junit.Test;

public class SquareTest {

    @Test
    public void getSideTest() {
        Square sqr = new Square(5);
        assertEquals("getSide gets the right value from the constructor ", 5 , sqr.getSide());
    }

    @Test
    public void getAreaTest() {
        Square sqr = new Square(5);
        assertEquals("getArea calculate correctly the area of square", 25 , sqr.getArea());
    }

    @Test
    public void toStringTest() {
        Square sqr = new Square(5);
        assertEquals("The square area is : " + sqr.getArea() , sqr.toString());
    }
}