package geometry;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import org.junit.Test;
public class RectangleTest {

    @Test
    public void constructorTest() {
        Rectangle rec = new Rectangle(7, 11);
        assertEquals("Constructor should set 11 as width", 11, rec.getWidth());
        assertEquals("Constructor should set 7 as length", 7, rec.getLength());
    }

    @Test
    public void gerAreaTest() {
        Rectangle rec = new Rectangle(2, 11);
        assertEquals("area should be set to 7*11", 2*11, rec.getArea());
    }

    @Test
    public void toStringTest() {
        Rectangle rec = new Rectangle(7, 2);
        assertEquals("toString should be the same", "length: "+rec.getLength()+", width: "+rec.getWidth()+", area: "+rec.getArea(), rec.toString());
    }
}
